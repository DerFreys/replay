package net.crackhd.replay.replay;

import lombok.Getter;
import lombok.Setter;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.recording.RecordTick;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.in.InStreamReader;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scoreboard.Scoreboard;
import org.spigotmc.TickLimiter;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 30.01.2017.
 */
public class ReplayManager {

    private Replay replay;
    private File file;

    private InStream inStream;
    private InStreamReader inStreamReader;

    public List<RecordTick> ticks = new ArrayList<>();

    @Getter private Scoreboard scoreboard;

    @Getter private int playedTicks = -1;
    private int version = 0;

    @Getter private World playedWorld;
    private UserHelper userHelper;

    @Setter @Getter private boolean running = true;
    @Getter private boolean replayStarted = false;

    public ReplayManager(Replay replay) {
        this.replay = replay;
        this.userHelper = new UserHelper(replay.getFakePlayerManager());
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public void loadReplay(String id) {
        replay.getLogger().info("Load replay with id " + id);
        file = new File("/home/replays/" + id + ".yml");
        try {
            inStream = new InStream(new DataInputStream(new FileInputStream(file)));
            inStreamReader = new InStreamReader();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        version = inStream.readInt();
        String worldName = inStream.readString();
        replay.getLogger().info("Replay has world " + worldName);

        WorldCreator worldCreator = new WorldCreator(worldName);
        this.playedWorld = worldCreator.createWorld();
        this.playedWorld.setTime(0);
        for(Entity entity : playedWorld.getEntities()) {
            if(entity instanceof LivingEntity && entity.getType() != EntityType.VILLAGER) {
                entity.remove();
            }
        }
        playedWorld.setGameRuleValue("doMobSpawning", "false");

        //Load ticks
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadTick();
            }
        }).start();
    }

    public void loadTick() {
        while(!inStream.isDone()) {
            if(inStream.readInt() == -1) {
                try {
                    RecordTick tick = new RecordTick();
                    tick.read(inStreamReader, inStream);
                    ticks.add(tick);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void startReplay() {
        replayStarted = true;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(replay, new Runnable() {
            @Override
            public void run() {
                userHelper.tick();
                if(isRunning()) {
                    RecordTick tick = getNextTick();
                    if (tick != null) {
                        for (Action action : tick.getActionList()) {
                            action.replay();
                        }
                    } else {
                        Bukkit.broadcastMessage(replay.getPrefix() + "§cDas Replay ist beendet");
                        Bukkit.broadcastMessage(replay.getPrefix() + "§cServerstop in 5 Sekunden");
                        setRunning(false);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(replay, new Runnable() {
                            @Override
                            public void run() {
                                Bukkit.shutdown();
                            }
                        }, 100L);
                    }
                }
            }
        }, 0L, 1L);
    }

    public void jumpTo(int seconds) {
        int tickCount = seconds * 20;
        if(tickCount - 1 >= ticks.size()) {
            return;
        }
        boolean wasRunning = running;
        setRunning(false);
        if(playedTicks == -1) {
            playedTicks = 0;
        }
        for(int i = playedTicks; i < tickCount; i++) {
            RecordTick tick = ticks.get(i);
            if(tick != null) {
                for (Action action : tick.getActionList()) {
                    action.replay();
                }
            }
        }
        playedTicks += (tickCount - playedTicks);
        userHelper.tick();
        setRunning(wasRunning);
    }

    public RecordTick getNextTick() {
        playedTicks++;
        if(playedTicks >= ticks.size()) {
            return null;
        }
        return ticks.get(playedTicks);
    }

    public void skip(int ticks) {
        this.playedTicks += ticks;
    }

}
