package net.crackhd.replay.replay;

import com.mojang.authlib.GameProfile;
import lombok.Getter;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Robin on 03.02.2017.
 */
public class FakePlayerManager {

    @Getter private Map<UUID, FakePlayer> fakePlayers = new HashMap<>();
    private Map<UUID, GameProfile> profiles = new HashMap<>();

    public GameProfile getProfile(UUID uuid) {
        if(profiles.containsKey(uuid)) {
            return profiles.get(uuid);
        }
        return new GameProfile(uuid, UUIDFetcher.getName(uuid));
    }

    public FakePlayer getFakePlayer(UUID uuid, World world) {
        if(fakePlayers.containsKey(uuid)) {
            return fakePlayers.get(uuid);
        }
        FakePlayer fakePlayer = new FakePlayer(this, uuid);
        fakePlayer.spawn(world);
        return fakePlayer;
    }

}
