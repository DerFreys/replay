package net.crackhd.replay.replay;

import lombok.Getter;
import net.crackhd.replay.replay.helper.FakePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Robin on 23.02.2017.
 */
public class UserHelper {

    private FakePlayerManager fakePlayerManager;
    private Map<Player, UserHelpEntry> userHelpEntryMap = new HashMap<>();

    public UserHelper(FakePlayerManager fakePlayerManager) {
        this.fakePlayerManager = fakePlayerManager;
    }

    public void tick() {
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(!userHelpEntryMap.containsKey(player)) {
                userHelpEntryMap.put(player, new UserHelpEntry());
            }
            checkPlayer(player);
        }
    }

    public void checkPlayer(Player player) {
        UserHelpEntry entry = userHelpEntryMap.get(player);
        for(FakePlayer fakePlayer : fakePlayerManager.getFakePlayers().values()) {
            if(fakePlayer != null && fakePlayer.getLocation() != null) {
                if (fakePlayer.getLocation().getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                    if (fakePlayer.getLocation().distance(player.getLocation()) <= fakePlayer.getEntityPlayer().getWorld().spigotConfig.playerTrackingRange) {
                        if(!entry.getVisiblePlayers().contains(fakePlayer.getGameProfile().getName())) {
                            entry.getVisiblePlayers().add(fakePlayer.getGameProfile().getName());
                            fakePlayer.spawnFor(player);
                        }
                    }else{
                        if(entry.getVisiblePlayers().contains(fakePlayer.getGameProfile().getName())) {
                            entry.getVisiblePlayers().remove(fakePlayer.getGameProfile().getName());
                            fakePlayer.despawnFor(player);
                        }
                    }
                }
            }
        }
    }

    @Getter
    public class UserHelpEntry {

        private List<String> visiblePlayers = new ArrayList<>();

    }

}
