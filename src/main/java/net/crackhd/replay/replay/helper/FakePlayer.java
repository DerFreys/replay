package net.crackhd.replay.replay.helper;

import com.mojang.authlib.GameProfile;
import lombok.Getter;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityTeleport;
import net.crackhd.replay.replay.FakePlayerManager;
import net.crackhd.replay.util.UUIDFetcher;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by Robin on 01.02.2017.
 */
@Getter
public class FakePlayer  {

    private FakePlayerManager fakePlayerManager;

    private UUID uuid;
    private int entityId;
    private GameProfile gameProfile;
    private Location location;
    private EntityPlayer entityPlayer;

    private boolean invisible = false;

    public FakePlayer(FakePlayerManager fakePlayerManager, UUID uuid) {
        fakePlayerManager.getFakePlayers().put(uuid, this);
        this.fakePlayerManager = fakePlayerManager;
        this.uuid = uuid;

        //Get the gameProfile
        gameProfile = new GameProfile(uuid, UUIDFetcher.getName(uuid));
        try {
            gameProfile = TileEntitySkull.skinCache.get(gameProfile.getName());
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void spawn(World world) {
        if(!invisible) {
            this.location = new Location(world, 0.0D, 0.0D, 0.0D);
            WorldServer craftWorld = ((CraftWorld) world).getHandle();
            entityPlayer = new EntityPlayer(MinecraftServer.getServer(), craftWorld, gameProfile, new PlayerInteractManager(craftWorld));
            entityPlayer.setPosition(0.0D, 0.0D, 0.0D);
            entityPlayer.yaw = 0.0F;
            entityPlayer.pitch = 0.0F;
            this.entityId = entityPlayer.getId();
            for (Player player : Bukkit.getOnlinePlayers()) {
                sendPacket(player, new PacketPlayOutNamedEntitySpawn(entityPlayer));
                sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
            }
        }
    }

    public void spawnFor(Player player) {
        if(!invisible) {
            sendPacket(player, new PacketPlayOutNamedEntitySpawn(entityPlayer));
            sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
            sendPacket(player, new PacketPlayOutEntityEquipment(entityId, 0, entityPlayer.inventory.getCarried()));
            sendPacket(player, new PacketPlayOutEntityEquipment(entityId, 1, entityPlayer.inventory.armor[0]));
            sendPacket(player, new PacketPlayOutEntityEquipment(entityId, 2, entityPlayer.inventory.armor[1]));
            sendPacket(player, new PacketPlayOutEntityEquipment(entityId, 3, entityPlayer.inventory.armor[2]));
            sendPacket(player, new PacketPlayOutEntityEquipment(entityId, 4, entityPlayer.inventory.armor[3]));
        }
    }

    public void despawn() {
        this.invisible = true;
        for(Player player : Bukkit.getOnlinePlayers()) {
            despawnFor(player);
            sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
        }
    }

    public void despawnFor(Player player) {
        sendPacket(player, new PacketPlayOutEntityDestroy(entityId));
    }

    public void teleport(Location location) {
        this.location = location;
        if(!invisible) {
            entityPlayer.setPosition(location.getX(), location.getY(), location.getZ());
            WrapperPlayServerEntityTeleport teleport = new WrapperPlayServerEntityTeleport();
            teleport.setEntityID(entityId);
            teleport.setX(location.getX());
            teleport.setY(location.getY());
            teleport.setZ(location.getZ());
            teleport.setPitch(location.getPitch());
            teleport.setYaw(location.getYaw());

            for (Player player : Bukkit.getOnlinePlayers()) {
                teleport.sendPacket(player);
            }
        }
    }

    public void equip(int slot, ItemStack itemStack) {
        if(slot == 0) {
            entityPlayer.inventory.setCarried(itemStack);
        }else{
            entityPlayer.inventory.armor[(slot - 1)] = itemStack;
        }

    }

    private void sendPacket(Player player, Packet packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

}
