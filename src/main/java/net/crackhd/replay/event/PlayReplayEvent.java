package net.crackhd.replay.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Robin on 05.04.2018.
 */
@AllArgsConstructor
@Getter
public class PlayReplayEvent extends Event {

    private String replayId;

    private static HandlerList handlerList = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

}
