package net.crackhd.replay.network;

/**
 * Created by Robin on 05.04.2018.
 */
public interface INetworkManager {

    void startReplay(String id);

    void completeReplay(String id);

}
