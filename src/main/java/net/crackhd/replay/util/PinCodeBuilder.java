package net.crackhd.replay.util;

import java.util.Random;

/**
 * Created by Robin on 31.01.2017.
 */
public class PinCodeBuilder {

    private static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private int groups;
    private int digits;
    private String pin;

    public PinCodeBuilder(){
        this.groups = 4;
        this.digits = 3;
        this.pin = generate();
    }

    public PinCodeBuilder groups(int groups){
        this.groups = groups;
        return this;
    }

    public PinCodeBuilder digits(int digits){
        this.digits = digits;
        return this;
    }

    public final String build(){
        this.pin = generate();
        return this.pin;
    }

    private String generate(){
        Random random;
        String pinCode = "";
        for(int i = 0; i < this.groups * this.digits; i++){
            random = new Random();
            pinCode += CHARS.charAt( random.nextInt(CHARS.length()) );
        }
        return pinCode;
    }
}
