package net.crackhd.replay.recording;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.in.InStreamReader;
import net.crackhd.replay.stream.out.OutStream;
import net.crackhd.replay.stream.out.OutStreamWriter;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 31.01.2017.
 */
@RequiredArgsConstructor
public class RecordTick {

    @Getter private List<Action> actionList = new ArrayList<>();

    public void addAction(Action action) {
        actionList.add(action);
    }

    public void write(OutStreamWriter outStreamWriter, OutStream outStream) {

        //Let the reader know that this is a new tick
        outStream.writeInt(-1);

        //Write size of actionList
        outStream.writeInt(actionList.size());

        //Write all actions
        for(Action action : actionList) {
            outStreamWriter.write(action, outStream);
        }

        //Flush the stream
        outStream.flush();
    }

    public void read(InStreamReader inStreamReader, InStream inStream) throws InstantiationException, IllegalAccessException {
        //Read size of actionList
        int size = inStream.readInt();

        for(int i = 0; i < size; i++) {
            Action action = inStreamReader.read(inStream);
            actionList.add(action);
        }
    }

}
