package net.crackhd.replay.recording;

import lombok.Getter;
import lombok.Setter;
import net.crackhd.replay.Replay;
import net.crackhd.replay.ReplayApi;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.network.INetworkManager;
import net.crackhd.replay.stream.out.OutStream;
import net.crackhd.replay.stream.out.OutStreamWriter;
import org.bukkit.Bukkit;
import org.bukkit.util.FileUtil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Robin on 30.01.2017.
 */
public class RecordingManager {

    private final Replay replay;
    private final INetworkManager networkManager;

    private File file;

    @Getter private boolean recording = false;

    private OutStream outStream;
    private OutStreamWriter outStreamWriter;

    private int version = 1;

    @Setter @Getter private boolean needsToBeSaved = true;

    private RecordTick currentTick;

    public RecordingManager(Replay replay, INetworkManager networkManager) {
        this.replay = replay;
        this.networkManager = networkManager;

        //Load the file and prepare the streams
        file = new File(replay.getDataFolder(), replay.getReplayId() + ".yml");
        try {
            replay.getDataFolder().mkdir();
            file.createNewFile();
            outStream = new OutStream(new DataOutputStream(new FileOutputStream(file)));
            outStreamWriter = new OutStreamWriter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the recording of the game here
     */
    public void startRecording() {
        replay.getLogger().info("Start recording...");

        //Let's inform the master about
        networkManager.startReplay(replay.getReplayId());

        this.recording = true;
        ReplayApi api = Replay.getInstance().getApi();

        //Write basic information for this replay
        outStream.writeInt(this.version);
        replay.getLogger().info("The played world is " + api.getWorld().getName());
        outStream.writeString(api.getWorld().getName());

        //Let's already switch the tick here to record the scoreboard, too
        switchTick();

        //Set a new RecordTick every tick and release the datas
        Bukkit.getScheduler().scheduleSyncRepeatingTask(replay, new Runnable() {
            @Override
            public void run() {
                if(recording) {
                    switchTick();
                }
            }
        }, 0L, 1L);
    }

    /**
     * Save the replay
     */
    public void save() {
        if(needsToBeSaved) {
            FileUtil.copy(file, new File("/home/replays/" + replay.getReplayId() + ".yml"));
            //Let's inform the master about that
            networkManager.completeReplay(replay.getReplayId());
        }
    }

    /**
     * Stop the recording of the game
     */
    public void stopRecording() {
        this.recording = false;
        replay.getLogger().info("Stop recording...");
    }

    /**
     * Set a new tick and write all actions of this tick
     */
    public void switchTick() {
        if(currentTick != null) {
            currentTick.write(outStreamWriter, outStream);
        }
        currentTick = new RecordTick();
    }

    /**
     * Handle an incoming action and add it to the tick
     * The action will be written with #switchTick()
     * @param action The called action
     */
    public void handleAction(Action action) {
        if(currentTick != null && recording) {
            currentTick.addAction(action);
        }
    }

}
