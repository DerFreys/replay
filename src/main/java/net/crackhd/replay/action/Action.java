package net.crackhd.replay.action;

import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;

/**
 * Created by Robin on 30.01.2017.
 */
public abstract class Action {

    public abstract void write(OutStream outStream);

    public abstract void read(InStream inStream);

    public abstract void replay();

}
