package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityEquipment;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Robin on 13.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class EquipmentAction extends Action {

    private UUID uuid;
    private ItemStack itemStack;
    private int slot;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeItemStack(itemStack);
        outStream.writeInt(slot);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.itemStack = inStream.readItemStack();
        this.slot = inStream.readInt();
    }

    @Override
    public void replay() {
        WrapperPlayServerEntityEquipment equipment = new WrapperPlayServerEntityEquipment();
        FakePlayer fakePlayer = Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, Replay.getInstance().getReplayManager().getPlayedWorld());
        fakePlayer.equip(slot, CraftItemStack.asNMSCopy(itemStack));

        if(!fakePlayer.isInvisible()) {
            equipment.setEntityID(fakePlayer.getEntityId());
            equipment.setItem(itemStack);
            equipment.setSlot(slot);

            for (Player online : Bukkit.getOnlinePlayers()) {
                equipment.sendPacket(online);
            }
        }
     }

}
