package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityStatus;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Robin on 13.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class StatusAction extends Action {

    private UUID uuid;
    private int status;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeInt(status);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.status = inStream.readInt();
    }

    @Override
    public void replay() {
        FakePlayer fakePlayer = Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, Replay.getInstance().getReplayManager().getPlayedWorld());
        if(!fakePlayer.isInvisible()) {
            WrapperPlayServerEntityStatus status = new WrapperPlayServerEntityStatus();
            status.setEntityID(fakePlayer.getEntityId());
            status.setEntityStatus((byte) this.status);
            for (Player online : Bukkit.getOnlinePlayers()) {
                status.sendPacket(online);
            }
        }
    }

}
