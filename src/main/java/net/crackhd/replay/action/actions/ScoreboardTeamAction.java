package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.replay.ReplayManager;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 14.04.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ScoreboardTeamAction extends Action {

    private String name;
    private String prefix;
    private String suffix;
    private String displayName;
    private String nameTagVisibility;
    private int color;
    private int mode;
    private int optionData;
    private List<String> players;

    @Override
    public void write(OutStream outStream) {
        outStream.writeString(name);
        outStream.writeString(prefix);
        outStream.writeString(suffix);
        outStream.writeString(displayName);
        outStream.writeString(nameTagVisibility);
        outStream.writeInt(color);
        outStream.writeInt(mode);
        outStream.writeInt(optionData);

        outStream.writeInt(players.size());
        for(String name : players) {
            outStream.writeString(name);
        }
    }

    @Override
    public void read(InStream inStream) {
        this.name = inStream.readString();
        this.prefix = inStream.readString();
        this.suffix = inStream.readString();
        this.displayName = inStream.readString();
        this.nameTagVisibility = inStream.readString();
        this.color = inStream.readInt();
        this.mode = inStream.readInt();
        this.optionData = inStream.readInt();

        this.players = new ArrayList<>();
        int size = inStream.readInt();
        for(int i = 0; i < size; i++) {
            players.add(inStream.readString());
        }
    }

    @Override
    public void replay() {
        ReplayManager replayManager = Replay.getInstance().getReplayManager();
        if(mode == 0) {
            Team team = replayManager.getScoreboard().getTeam(name);
            if(team == null) {
                team = replayManager.getScoreboard().registerNewTeam(name);
            }
            team.setDisplayName(displayName != null ? displayName : "");
            team.setPrefix(prefix != null ? prefix : "");
            team.setSuffix(suffix != null ? suffix : "");
            for (String player : players) {
                team.addEntry(player);
            }
        }else if(mode == 1) {
            //TODO Remove Team
        }else if(mode == 2) {
            Team team = replayManager.getScoreboard().getTeam(name);
            if(team == null) {
                team = replayManager.getScoreboard().registerNewTeam(name);
            }
            team.setDisplayName(displayName != null ? displayName : "");
            team.setPrefix(prefix != null ? prefix : "");
            team.setSuffix(suffix != null ? suffix : "");
        }else if(mode == 3) {
            Team team = replayManager.getScoreboard().getTeam(name);
            if(team == null) {
                team = replayManager.getScoreboard().registerNewTeam(name);
            }
            for(String player : players) {
                team.addEntry(player);
            }
        }
    }

}
