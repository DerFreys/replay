package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

/**
 * Created by Robin on 01.03.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ProjectileAction extends Action {

    private String world;
    private EntityType entityType;
    private Location location;
    private Vector velocity;

    @Override
    public void write(OutStream outStream) {
        outStream.writeString(world);
        outStream.writeString(entityType.name());
        outStream.writeLocation(location);
        outStream.writeVector(velocity);
    }

    @Override
    public void read(InStream inStream) {
        this.world = inStream.readString();
        this.entityType = EntityType.valueOf(inStream.readString());
        this.location = inStream.readLocation(world);
        this.velocity = inStream.readVector();
    }

    @Override
    public void replay() {
        Projectile projectile = (Projectile) location.getWorld().spawn(location, entityType.getEntityClass());
        projectile.setVelocity(velocity);
    }

}