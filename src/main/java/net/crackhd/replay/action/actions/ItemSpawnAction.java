package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 01.03.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ItemSpawnAction extends Action {

    private String world;
    private ItemStack itemStack;
    private Location location;

    @Override
    public void write(OutStream outStream) {
        outStream.writeString(world);
        outStream.writeItemStack(itemStack);
        outStream.writeLocation(location);
    }

    @Override
    public void read(InStream inStream) {
        world = inStream.readString();
        this.itemStack = inStream.readItemStack();
        this.location = inStream.readLocation(world);
    }

    @Override
    public void replay() {
        location.getWorld().dropItemNaturally(location, itemStack);
    }

}
