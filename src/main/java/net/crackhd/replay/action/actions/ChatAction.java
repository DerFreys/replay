package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;

import java.util.UUID;

/**
 * Created by Robin on 10.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ChatAction extends Action {

    private UUID uuid;
    private String message;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeString(message);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.message = inStream.readString();
    }

    @Override
    public void replay() {
        //Bukkit.broadcastMessage("<" + Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, Bukkit.getWorlds().get(0)).getGameProfile().getName() + "> " + message);
    }

}
