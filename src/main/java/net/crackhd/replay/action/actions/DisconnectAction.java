package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;

import java.util.UUID;

/**
 * Created by Robin on 27.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class DisconnectAction extends Action {

    private UUID uuid;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
    }

    @Override
    public void replay() {
        FakePlayer fakePlayer = Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, Replay.getInstance().getReplayManager().getPlayedWorld());
        if(fakePlayer != null) {
            fakePlayer.despawn();
        }
    }

}
