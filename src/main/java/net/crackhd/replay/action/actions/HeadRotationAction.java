package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityHeadRotation;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Robin on 10.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class HeadRotationAction extends Action {

    private UUID uuid;
    private float headYaw;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeFloat(headYaw);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.headYaw = inStream.readFloat();
    }

    @Override
    public void replay() {
        WrapperPlayServerEntityHeadRotation rotation = new WrapperPlayServerEntityHeadRotation();
        FakePlayer fakePlayer = Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, Replay.getInstance().getReplayManager().getPlayedWorld());
        rotation.setEntityID(fakePlayer.getEntityId());
        rotation.setHeadYaw((byte)(int)(this.headYaw * 256.0F / 360.0F));

        for(Player player : Bukkit.getOnlinePlayers()) {
            rotation.sendPacket(player);
        }
    }

}
