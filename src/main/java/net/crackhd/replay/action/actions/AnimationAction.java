package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.packetwrapper.WrapperPlayServerAnimation;
import net.crackhd.replay.replay.FakePlayerManager;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Robin on 01.02.2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class AnimationAction extends Action {

    private UUID uuid;
    private int animation;
    private String world;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeInt(animation);
        outStream.writeString(world);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.animation = inStream.readInt();
        this.world = inStream.readString();
    }

    @Override
    public void replay() {
        FakePlayerManager fakePlayerManager = Replay.getInstance().getFakePlayerManager();
        FakePlayer fakePlayer = fakePlayerManager.getFakePlayer(uuid, Bukkit.getWorld(world));
        if(!fakePlayer.isInvisible()) {
            WrapperPlayServerAnimation animation = new WrapperPlayServerAnimation();
            animation.setEntityID(fakePlayer.getEntityId());
            animation.setAnimation(this.animation);

            for (Player player : Bukkit.getOnlinePlayers()) {
                animation.sendPacket(player);
            }
        }
    }

}
