package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.replay.helper.FakePlayer;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Location;

import java.util.UUID;

/**
 * Created by Robin on 30.01.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class MoveAction extends Action {

    private UUID uuid;
    private Location location;

    @Override
    public void write(OutStream outStream) {
        outStream.writeUUID(uuid);
        outStream.writeLocation(location);
    }

    @Override
    public void read(InStream inStream) {
        this.uuid = inStream.readUUID();
        this.location = inStream.readLocation(Replay.getInstance().getReplayManager().getPlayedWorld().getName());
    }

    @Override
    public void replay() {
        FakePlayer fakePlayer = Replay.getInstance().getFakePlayerManager().getFakePlayer(uuid, location.getWorld());
        fakePlayer.teleport(location);
    }

}
