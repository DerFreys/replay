package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * Created by Robin on 10.02.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class BlockPlaceAction extends Action {

    private Location location;
    private Material material;
    private byte data;

    @Override
    public void write(OutStream outStream) {
        outStream.writeString(location.getWorld().getName());
        outStream.writeLocation(location);
        outStream.writeString(material.toString());
        outStream.writeByte(data);
    }

    @Override
    public void read(InStream inStream) {
        String world = inStream.readString();
        this.location = inStream.readLocation(world);
        this.material = Material.valueOf(inStream.readString());
        this.data = inStream.readByte();
    }

    @Override
    public void replay() {
        location.getBlock().setType(material);
        location.getBlock().setData(data);
    }

}
