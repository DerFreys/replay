package net.crackhd.replay.action.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.crackhd.replay.action.Action;
import net.crackhd.replay.stream.in.InStream;
import net.crackhd.replay.stream.out.OutStream;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * Created by Robin on 10.02.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class BlockBreakAction extends Action {

    private Location location;

    @Override
    public void write(OutStream outStream) {
        outStream.writeString(location.getWorld().getName());
        outStream.writeLocation(location);
    }

    @Override
    public void read(InStream inStream) {
        String world = inStream.readString();
        this.location = inStream.readLocation(world);
    }

    @Override
    public void replay() {
        location.getBlock().setType(Material.AIR);
    }

}