package net.crackhd.replay.action;

import com.google.common.reflect.ClassPath;

import java.util.HashMap;

/**
 * Created by Robin on 31.01.2017.
 */
public class ActionProtocol {

    private static HashMap<Integer, Class<? extends Action>> actionInteger = new HashMap<>();
    private static HashMap<Class<? extends Action>, Integer> actionClasses = new HashMap<>();

    /**
     * Register all the actions
     * @param instance An Class to have an ClassLoader
     */
    public static void registerActions(Class instance) {
        try {
            for(ClassPath.ClassInfo classInfo : ClassPath.from(instance.getClassLoader()).getTopLevelClassesRecursive("net.crackhd.replay.action.actions")) {
                Class c = Class.forName(classInfo.getName(), true, instance.getClassLoader());
                Integer packetId = classInfo.getSimpleName().hashCode();
                Action action = (Action) c.newInstance();
                if(actionInteger.containsKey(packetId)) {
                    throw new IllegalStateException("There's already a packet registered with id " + packetId);
                }
                actionInteger.put(packetId, action.getClass());
                actionClasses.put(action.getClass(), packetId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get an action class
     * @param id Id of the action class
     * @return Action The action class of the id
     */
    public static Class<? extends Action> getClass(int id) {
        return actionInteger.get(id);
    }

    /**
     * Get the id of the action class
     * @param action The action we want the id from
     * @return int The id of the class
     */
    public static int getActionID(Class<? extends Action> action) {
        return actionClasses.get(action);
    }

}
