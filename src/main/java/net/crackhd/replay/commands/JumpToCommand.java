package net.crackhd.replay.commands;

import lombok.AllArgsConstructor;
import net.crackhd.replay.Replay;
import net.crackhd.replay.replay.ReplayManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Robin on 01.03.2017.
 */
@AllArgsConstructor
public class JumpToCommand implements CommandExecutor {

    private Replay replay;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage("Only available for players");
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(replay.getPrefix() + "§cNutze /jumpto <Sekunde>");
            return true;
        }
        try{
            int second = Integer.valueOf(args[0]);
            int ticks = second * 20;
            if(replay.getReplayManager().getPlayedTicks() > ticks) {
                sender.sendMessage(replay.getPrefix() + "§cDu kannst zur Zeit nur vorwärts springen!");
                return true;
            }
            replay.getReplayManager().jumpTo(second);
            sender.sendMessage(replay.getPrefix() + "§7Du bist zu Sekunde §e" + second + " §7gesprungen");
        }catch(NumberFormatException ex) {
            sender.sendMessage(replay.getPrefix() + "§cBitte gebe eine gültige Zahl ein!");
        }
        return true;
    }

}
