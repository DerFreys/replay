package net.crackhd.replay.stream.out;

import net.crackhd.replay.action.Action;
import net.crackhd.replay.action.ActionProtocol;

/**
 * Created by Robin on 31.01.2017.
 */
public class OutStreamWriter {

    public void write(Action action, OutStream outStream) {
        //Get the actionId of this Action
        int actionId = ActionProtocol.getActionID(action.getClass());
        if(actionId == -1) {
            throw new RuntimeException("No ID found for Packet " + action.getClass().toString());
        }

        //First write the id of the action to know which action we have whilst reading and then write the action
        outStream.writeInt(ActionProtocol.getActionID(action.getClass()));
        action.write(outStream);
    }

}
