package net.crackhd.replay.stream.out;

import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Robin on 30.01.2017.
 */
@RequiredArgsConstructor
public class OutStream {

    private final DataOutputStream outputStream;

    public void writeByte(byte object) {
        try {
            outputStream.writeByte(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeInt(int object) {
        try {
            outputStream.writeInt(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeString(String string) {
        try {
            outputStream.writeUTF(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeDouble(double object) {
        try {
            outputStream.writeDouble(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFloat(float object) {
        try {
            outputStream.writeFloat(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeLong(long object) {
        try {
            outputStream.writeLong(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeLocation(Location location) {
        try {
            outputStream.writeDouble(location.getX());
            outputStream.writeDouble(location.getY());
            outputStream.writeDouble(location.getZ());
            outputStream.writeFloat(location.getYaw());
            outputStream.writeFloat(location.getPitch());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeUUID(UUID uuid) {
        try {
            outputStream.writeLong(uuid.getMostSignificantBits());
            outputStream.writeLong(uuid.getLeastSignificantBits());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeItemStack(ItemStack itemStack) {
        try {
            writeString(itemStack.getType().toString());
            writeInt(itemStack.getAmount());
            outputStream.writeShort(itemStack.getDurability());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeVector(Vector vector) {
        try {
            outputStream.writeDouble(vector.getX());
            outputStream.writeDouble(vector.getY());
            outputStream.writeDouble(vector.getY());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void flush() {
        try {
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
