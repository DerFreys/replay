package net.crackhd.replay.stream.in;

import net.crackhd.replay.action.Action;
import net.crackhd.replay.action.ActionProtocol;

/**
 * Created by Robin on 31.01.2017.
 */
public class InStreamReader {

    public Action read(InStream inStream) throws IllegalAccessException, InstantiationException {
        int packetId = inStream.readInt();

        Class<? extends Action> actionClass = ActionProtocol.getClass(packetId);
        if(actionClass != null) {
            Action action = actionClass.newInstance();
            action.read(inStream);
            return action;
        }
        return null;
    }

}
