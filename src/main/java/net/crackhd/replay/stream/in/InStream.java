package net.crackhd.replay.stream.in;

import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Robin on 30.01.2017.
 */
@RequiredArgsConstructor
public class InStream {

    private final DataInputStream inputStream;

    public boolean isDone() {
        try {
            return inputStream.available() <= 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public byte readByte() {
        try {
            return inputStream.readByte();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int readInt() {
        try {
            return inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String readString() {
        try {
            return inputStream.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public double readDouble() {
        try {
            return inputStream.readDouble();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public float readFloat() {
        try {
            return inputStream.readFloat();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readLong() {
        try {
            return inputStream.readLong();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Location readLocation(String world) {
        try {
            double x = inputStream.readDouble();
            double y = inputStream.readDouble();
            double z = inputStream.readDouble();
            float yaw = inputStream.readFloat();
            float pitch = inputStream.readFloat();
            return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UUID readUUID() {
        try {
            return new UUID(inputStream.readLong(), inputStream.readLong());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ItemStack readItemStack() {
        try {
            Material material = Material.valueOf(readString());
            int amount = readInt();
            short damage = inputStream.readShort();
            return new ItemStack(material, amount, damage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vector readVector() {
        try {
            double x = inputStream.readDouble();
            double y = inputStream.readDouble();
            double z = inputStream.readDouble();
            return new Vector(x, y, z);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}