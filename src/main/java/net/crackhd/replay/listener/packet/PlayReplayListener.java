package net.crackhd.replay.listener.packet;

import lombok.AllArgsConstructor;
import net.crackhd.replay.Replay;
import net.crackhd.replay.event.PlayReplayEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.function.Consumer;

/**
 * Created by Robin on 05.04.2018.
 */
@AllArgsConstructor
public class PlayReplayListener implements Listener {

    private Replay replay;

    @EventHandler
    public void onPlayReplay(PlayReplayEvent event) {
        replay.getConsumers().add(new Consumer<Void>() {
            @Override
            public void accept(Void aVoid) {
                replay.getReplayManager().loadReplay(event.getReplayId());
                replay.getLogger().info("Load replay " + event.getReplayId());
            }
        });
    }

}
