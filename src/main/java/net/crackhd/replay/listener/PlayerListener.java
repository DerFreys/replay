package net.crackhd.replay.listener;

import lombok.AllArgsConstructor;
import net.crackhd.replay.action.actions.*;
import net.crackhd.replay.recording.RecordingManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Robin on 30.01.2017.
 */
@AllArgsConstructor
public class PlayerListener implements Listener {

    private RecordingManager recordingManager;

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        BlockPlaceAction action = new BlockPlaceAction(event.getBlock().getLocation(), event.getBlockPlaced().getType(), event.getBlockPlaced().getData());
        recordingManager.handleAction(action);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        BlockBreakAction action = new BlockBreakAction(event.getBlock().getLocation());
        recordingManager.handleAction(action);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        recordingManager.handleAction(new ChatAction(event.getPlayer().getUniqueId(), event.getMessage()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        recordingManager.handleAction(new DisconnectAction(event.getPlayer().getUniqueId()));
    }

    @EventHandler
    public void onSpawn(ItemSpawnEvent event) {
        //recordingManager.handleAction(new ItemSpawnAction(event.getEntity().getWorld().getName(), event.getEntity().getItemStack(), event.getLocation()));
    }

    @EventHandler
    public void onLaunch(ProjectileLaunchEvent event) {
        recordingManager.handleAction(new ProjectileAction(event.getEntity().getWorld().getName(),
                event.getEntityType(), event.getEntity().getLocation(), event.getEntity().getVelocity()));
    }

}
