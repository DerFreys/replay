package net.crackhd.replay.listener;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.crackhd.replay.Replay;
import net.crackhd.replay.action.actions.AnimationAction;
import net.crackhd.replay.action.actions.EquipmentAction;
import net.crackhd.replay.action.actions.ScoreboardTeamAction;
import net.crackhd.replay.action.actions.StatusAction;
import net.crackhd.replay.packetwrapper.WrapperPlayServerAnimation;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityEquipment;
import net.crackhd.replay.packetwrapper.WrapperPlayServerEntityStatus;
import net.crackhd.replay.packetwrapper.WrapperPlayServerScoreboardTeam;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Robin on 01.02.2017.
 */
public class ReplayPacketListener extends PacketAdapter{

    private Replay replay;

    public ReplayPacketListener(Replay replay) {
        super(replay, PacketType.Play.Server.ENTITY_STATUS, PacketType.Play.Server.ANIMATION, PacketType.Play.Server.ENTITY_EQUIPMENT, PacketType.Play.Server.SCOREBOARD_TEAM,
                PacketType.Play.Client.ENTITY_ACTION);
        this.replay = replay;
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        if(event.getPacketType() == PacketType.Play.Server.ANIMATION) {
            WrapperPlayServerAnimation animation = new WrapperPlayServerAnimation(event.getPacket());
            if (animation.getEntity(event) instanceof Player) {
                UUID uuid = animation.getEntity(event).getUniqueId();
                World world = animation.getEntity(event).getWorld();
                replay.getRecordingManager().handleAction(new AnimationAction(uuid, animation.getAnimation(), world.getName()));
            }
        }else if(event.getPacketType() == PacketType.Play.Server.ENTITY_STATUS) {
            WrapperPlayServerEntityStatus status = new WrapperPlayServerEntityStatus(event.getPacket());
            if(status.getEntity(event) instanceof Player) {
                replay.getRecordingManager().handleAction(new StatusAction(status.getEntity(event).getUniqueId(), status.getEntityStatus()));
            }
        }else if(event.getPacketType() == PacketType.Play.Server.ENTITY_EQUIPMENT) {
            WrapperPlayServerEntityEquipment equipment = new WrapperPlayServerEntityEquipment(event.getPacket());
            if(equipment.getEntity(event) instanceof Player) {
                replay.getRecordingManager().handleAction(new EquipmentAction(equipment.getEntity(event).getUniqueId(), equipment.getItem(), equipment.getSlot()));
            }
        }else if(event.getPacketType() == PacketType.Play.Server.SCOREBOARD_TEAM) {
            //We only set the scoreboard of one person
            if(!event.getPlayer().getUniqueId().equals(replay.getApi().getScoreboardTarget())) {
                return;
            }
            WrapperPlayServerScoreboardTeam scoreboardTeam = new WrapperPlayServerScoreboardTeam(event.getPacket());
            ScoreboardTeamAction meta = new ScoreboardTeamAction(scoreboardTeam.getName(), scoreboardTeam.getPrefix(), scoreboardTeam.getSuffix(), scoreboardTeam.getDisplayName(), scoreboardTeam.getNameTagVisibility(),
                    scoreboardTeam.getColor(), scoreboardTeam.getMode(), scoreboardTeam.getPackOptionData(), scoreboardTeam.getPlayers());
            replay.getRecordingManager().handleAction(meta);
        }
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {

    }

}
