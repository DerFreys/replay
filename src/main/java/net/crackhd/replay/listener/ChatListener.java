package net.crackhd.replay.listener;

import lombok.AllArgsConstructor;
import net.crackhd.replay.Replay;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Robin on 26.11.2017.
 */
@AllArgsConstructor
public class ChatListener implements Listener {

    private Replay replay;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        event.setFormat(replay.getPrefix() + "§e" + event.getPlayer().getName() + " §8\u00bb §7" + event.getMessage());
    }

}
