package net.crackhd.replay.listener;

import lombok.AllArgsConstructor;
import net.crackhd.replay.Replay;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Robin on 26.02.2017.
 */
@AllArgsConstructor
public class QuitListener implements Listener {

    private Replay replay;

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(replay.getPrefix() + "§6" + event.getPlayer().getName() + " §7hat den Server verlassen");
    }

}
