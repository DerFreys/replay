package net.crackhd.replay.listener;

import lombok.AllArgsConstructor;
import net.crackhd.replay.Replay;
import net.crackhd.replay.replay.ReplayManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Team;

/**
 * Created by Robin on 13.02.2017.
 */
@AllArgsConstructor
public class JoinListener implements Listener {

    private Replay replay;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        event.setJoinMessage(replay.getPrefix() + "§6" + player.getName() + " §7hat den Server betreten");
        player.teleport(replay.getReplayManager().getPlayedWorld().getSpawnLocation());
        player.setAllowFlight(true);
        player.setFlying(true);

        //Set the items
        //player.getInventory().setItem(0, replay.getTeleporterItem().getItemStack());
        //player.getInventory().setItem(3, replay.getRewindItem().getItemStack());
        //player.getInventory().setItem(4, replay.getStartItem().getItemStack());
        //player.getInventory().setItem(5, replay.getFastForwardItem().getItemStack());
        //player.getInventory().setItem(8, replay.getAbortItem().getItemStack());

        //Set the scoreboard in here
        player.setScoreboard(replay.getReplayManager().getScoreboard());

        Team team = replay.getReplayManager().getScoreboard().getTeam("replay");
        if(team == null) {
            team = replay.getReplayManager().getScoreboard().registerNewTeam("replay");
            team.setPrefix("§7Replay §7| ");
        }

        team.addEntry(player.getName());
    }

}
