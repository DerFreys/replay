package net.crackhd.replay.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Created by Robin on 27.02.2017.
 */
public class LoginListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(PlayerLoginEvent event) {
        if(!event.getPlayer().hasPermission("network.command.replay")) {
            event.setKickMessage("§cDu darfst diesen Server nicht betreten");
            event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
        }
    }

}
