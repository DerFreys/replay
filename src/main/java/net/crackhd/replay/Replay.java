package net.crackhd.replay;

import com.comphenix.protocol.ProtocolLibrary;
import lombok.Getter;
import net.crackhd.replay.action.ActionProtocol;
import net.crackhd.replay.action.actions.HeadRotationAction;
import net.crackhd.replay.action.actions.MoveAction;
import net.crackhd.replay.commands.JumpToCommand;
import net.crackhd.replay.listener.*;
import net.crackhd.replay.listener.packet.PlayReplayListener;
import net.crackhd.replay.recording.RecordingManager;
import net.crackhd.replay.replay.FakePlayerManager;
import net.crackhd.replay.replay.ReplayManager;
import net.crackhd.replay.util.PinCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Robin on 30.01.2017.
 */
@Getter
public class Replay extends JavaPlugin {

    @Getter private static Replay instance;

    private final String prefix = " §aReplay §8⎟ §7";

    private RecordingManager recordingManager;

    private ReplayManager replayManager;
    private FakePlayerManager fakePlayerManager;

    private ReplayApi api;

    private String replayId;

    private List<Consumer<Void>> consumers = new ArrayList<>();

    @Override
    public void onLoad() {
        Bukkit.getPluginManager().registerEvents(new PlayReplayListener(this), this);
    }

    @Override
    public void onEnable() {
        instance = this;

        //Register Actions
        ActionProtocol.registerActions(this.getClass());

        if(System.getProperty("replay") != null) {
            getLogger().info("This is a replay server");
            fakePlayerManager = new FakePlayerManager();
            replayManager = new ReplayManager(this);

            //This is a little bit cheaty and should be replaced
            for(Consumer<Void> consumer : consumers) {
                consumer.accept(null);
            }

            Bukkit.getPluginManager().registerEvents(new JoinListener(this), this);
            Bukkit.getPluginManager().registerEvents(new EntityDamageListener(), this);
            Bukkit.getPluginManager().registerEvents(new FoodLevelChangeListener(), this);
            Bukkit.getPluginManager().registerEvents(new QuitListener(this), this);
            Bukkit.getPluginManager().registerEvents(new LoginListener(), this);
            Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
            Bukkit.getPluginManager().registerEvents(new ChatListener(this), this);

            getCommand("jumpto").setExecutor(new JumpToCommand(this));

            //Register items
            /*Items items = HandlerFrame.getInstance().getPlugin("items");
            teleporterItem = new TeleporterItem(fakePlayerManager);
            startItem = new StartItem(this);
            stopItem = new StopItem(this);
            abortItem = new AbortItem(this);
            fastForwardItem = new FastForwardItem(this);
            rewindItem = new RewindItem(this);

            items.getItems().add(teleporterItem);
            items.getItems().add(startItem);
            items.getItems().add(stopItem);
            items.getItems().add(abortItem);
            items.getItems().add(fastForwardItem);
            items.getItems().add(rewindItem);*/

            return;
        }
        getLogger().info("We record this server");

        //Generate replay id
        replayId = new PinCodeBuilder().groups(2).digits(4).build();

        //Init RecordingManager
        //TODO Implement network manager
        recordingManager = new RecordingManager(this, null);

        //Load api
        api = new ReplayApi(recordingManager);

        //Load events
        ProtocolLibrary.getProtocolManager().addPacketListener(new ReplayPacketListener(this));
        Bukkit.getPluginManager().registerEvents(new PlayerListener(recordingManager), this);


        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if(recordingManager.isRecording()) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        recordingManager.handleAction(new MoveAction(player.getUniqueId(), player.getLocation()));
                        recordingManager.handleAction(new HeadRotationAction(player.getUniqueId(), player.getEyeLocation().getYaw()));
                    }
                }
            }
        }, 1L, 1L);
    }

    @Override
    public void onDisable() {
        if(recordingManager != null) {
            recordingManager.save();
        }
    }

}
