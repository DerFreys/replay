package net.crackhd.replay;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.crackhd.replay.action.actions.DisconnectAction;
import net.crackhd.replay.recording.RecordingManager;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Robin on 11.02.2017.
 */
@RequiredArgsConstructor
public class ReplayApi {

    private final RecordingManager recordingManager;

    @Getter private World world;

    @Getter private UUID scoreboardTarget;

    /**
     * Start the record
     */
    public void startRecording() {
        recordingManager.startRecording();
    }

    /**
     * Stop the record
     */
    public void stopRecording() {
        recordingManager.stopRecording();

        recordingManager.save();
    }

    /**
     * Set the map
     * @param world The world which is loaded
     */
    public void setMap(World world) {
        this.world = world;
    }

    /**
     * Set the boolean if this replay needs to be saved
     * @param needsToBeSaved boolean if this replay needs to saved
     */
    public void setNeedsToBeSaved(boolean needsToBeSaved) {
        recordingManager.setNeedsToBeSaved(needsToBeSaved);
    }

    /**
     * Remove a player from the replay
     * @param player The player who left/died
     */
    public void removePlayer(Player player) {
        recordingManager.handleAction(new DisconnectAction(player.getUniqueId()));
    }

    /**
     * Set the player of which the scoreboard will be displayed
     * @param uuid The uuid of the player
     */
    public void setScoreboardTarget(UUID uuid) {
       this.scoreboardTarget = uuid;
    }

}
